<?php

$ROUTE['/'] = 'core/index';
$ROUTE['/add'] = 'core/appendItem';
$ROUTE['/redo/(\d*)'] = 'core/redoItem/$1';
$ROUTE['/relate/(\d*)'] = 'core/loadRelate/$1';
$ROUTE['/del/relate/(\d*)'] = 'core/deleteRelate/$1';
$ROUTE['/load_variant'] = 'core/variantList';
$ROUTE['/one/(\d*)'] = 'core/getElement/$1';
$ROUTE['/all'] = 'core/loadAll';


require_once "functions/function.php";