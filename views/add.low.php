<head>
    <title>TEST</title>
    <link rel="stylesheet" href="/bootstrap.min.css">
    <script src="/jq.js"></script>
</head>
<dody>
    <h1 style="text-align: center"><? if ($data['id']) { ?>REDO<? } else { ?>APPEND<? } ?> Item
    </h1>
    <hr>
    <div class="container">
        <form action="<? if ($data['id']) { ?>/redo<? } else { ?>/add<? } ?>" method="post" class="form-horizontal"
              id="add">
            <input type="hidden" name="id" value="<?= $data['id'] ?>">
            <div class="form-group">
                <label for="name">Input name:</label>
                <input type="text" name="name" class="form-control" required value="<?= $data['name'] ?>">
            </div>
            <div class="form-group">
                <label for="cost">Input cost:</label>
                <input type="text" name="cost" class="form-control" required value="<?= $data['cost'] ?>">
            </div>
            <span class="btn btn-warning" id="add_type">+</span>
            <div class="types">

            </div>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
    <script>
        var item_index = 0;

        var item = function (type, el) {
            var value = '';
            if (el) {
                if (el.value) {
                    value = el.value;
                }
            }

            if (type == '') {
                return '';
            }
            if (type == 2) {
                return '<label for="type-input['+item_index+']">input content</label>' +
                    '<textarea name="type-input['+item_index+']" class="form-control" required >' + value + '</textarea>'
            }
            if (type == 'checkbox') {
                var checked = '';
                if (value) {
                    checked = 'checked';
                }
                return '<label for="type-input['+item_index+']">input content</label>' +
                    '<input type="' + type + '" name="type-input['+item_index+']" class="form-control" ' + checked + '>';
            }
            if (type == 'selector') {
                return '<label for="type-input['+item_index+']">input content</label>' +
                    '<select name="type-input['+item_index+']" class="form-control" required>' +
                    '</select>';
            }
            return '<label for="type-input['+item_index+']">input content</label>' +
                '<input type="' + type + '" name="type-input['+item_index+']" class="form-control" required value="' + value + '">';
        }

        var type_P = function (el) {
            item_index++;
            var id = '';
            var type = '';
            var name = '';
            if (el) {
                if (el.id) {
                    id = el.id;
                }
                if (el.name) {
                    name = el.name;
                }
                if (el.type_id == '1') {
                    type = 'text';
                } else if (el.type_id == '2') {
                    type = 2
                } else if (el.type_id == '3') {
                    type = 'checkbox';
                } else if (el.type_id == '4') {
                    type = 'number';
                } else if (el.type_id == '5') {
                    type = 'selector';
                }
            }
            var insert = item(type, el);

            return '<div class="panel panel-info">' +
                '<div class="panel-heading">' +
                '<input type="hidden" value="' + id + '" name="relate_id['+item_index+']">' +
                '<label for="type['+item_index+']">SELECT type:</label>' +
                '<select name="type['+item_index+']" class="form-control type_select" required >' +
                '<option value="0" selected>none</option>' +
                '<option value="1">text</option>' +
                '<option value="2">long-text</option>' +
                '<option value="3">Y/N</option>' +
                '<option value="4">Int Value</option>' +
                '<option value="5">Selector</option>' +
                '</select>' +
                '<input type="text" class="form-control" name="relate_name['+item_index+']" value="'+name+
            '">' +
            '<span class="btn btn-danger delete" datatype="' + id + '">X</span> ' +
            '</div>' +
            '<div class="panel-body">' +
            '<div class="form-group type_mod">' +
            insert +
            '</div>' +
            '</div>' +
            '</div>'
        }

        var changer = function (self) {
            var parent = self.closest('.panel');
            var val = self.val(), type = '';
            parent.find('.type_mod').empty();
            if (val == 1) {
                type = 'text';
            } else if (val == 2) {
                type = 2
            } else if (val == 3) {
                type = 'checkbox';
            } else if (val == 4) {
                type = 'number';
            } else if (val == 5) {
                type = 'selector';
            }
            parent.find('.type_mod').append(item(type, null));
            if (val == 5) {
                var select = parent.find('.type_mod').find('select');
                $.ajax({
                    url: "/load_variant",
                    dataType: "json",
                    success: function (data) {
                        $.each(data, function (k, v) {
                            select.append('<option value="' + k + '">' + v + '</option>');
                        });
                    }
                });
            }
        }

        var deleter = function (self) {
            var parent = self.closest('.panel');
            var id = self.attr('datatype');
            if (confirm('Are you sure?')) {
                parent.hide('slow').remove();
                if (id) {
                    $.ajax({
                        url: "/del/relate/" + id,
                        type: "post",
                        dataType: 'json',
                        success: function () {
                            alert('deleted from base');
                        }
                    });
                }
            }
        }

        $(document).ready(function () {
            var $types = $('.types');

            $('.type_select').on('change', function () {
                var parent = $(this).closest('.panel');
                var val = $(this).val(), type = '';
                parent.find('.type_mod').empty();
                if (val == 1) {
                    type = 'text';
                } else if (val == 2) {
                    type = 2
                } else if (val == 3) {
                    type = 'checkbox';
                } else if (val == 4) {
                    type = 'number';
                } else if (val == 5) {
                    type = 'selector';
                }
                parent.find('.type_mod').append(item(type, null));
                if (val == 5) {
                    var select = parent.find('.type_mod').find('select');
                    $.ajax({
                        url: "/load_variant",
                        dataType: "json",
                        success: function (data) {
                            $.each(data, function (k, v) {
                                select.append('<option value="' + k + '">' + v + '</option>');
                            });
                        }
                    });
                }
            });

            $('#add_type').on('click', function () {
                $types.append(type_P(null));
                $('.panel').last().find('select').on('change', function () {
                    changer($(this));
                })
                $('.panel').last().find('.delete').on('click', function () {
                    deleter($(this));
                });
            });

            $('#add').on('submit', function () {
                var data = $(this).serializeArray();
                var method = '/add';
                <?if($data['id']):?>
                    method = '/redo/<?=$data['id']?>';
                <?endif;?>
                $.ajax({
                    url: method,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        if (response.goto) {
                            alert('Saved');
                            setTimeout(function () {
                                $(location).attr('href', response.goto);
                            }, 1500)
                        } else if(response.error){
                            alert(response.error);
                        }
                    },
                    error: function () {

                    }
                });
                return false;
            });

            <?if($data['id']):?>
            $.ajax({
                url: "/relate/<?=$data['id']?>",
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        $.each(data, function (k, v) {
                            $types.append(type_P(v));
                            $('.panel').last().find('select').val(v.type_id);
                            $('.panel').last().find('.panel-heading').find('select').on('change', function () {
                                changer($(this));
                            });
                            $('.panel').last().find('.delete').on('click', function () {
                                deleter($(this));
                            });
                            if (v.type_id == 5) {
                                var select = $('.panel').last().find('.type_mod').find('select');
                                $.ajax({
                                    url: "/load_variant",
                                    dataType: "json",
                                    success: function (data) {
                                        $.each(data, function (k, val) {
                                            var selected = '';
                                            if (k == v.value) {
                                                selected = 'selected';
                                            }
                                            select.append('<option value="' + k + '" ' + selected + '>' + val + '</option>');
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });
            <?endif;?>
        });
    </script>
</dody>