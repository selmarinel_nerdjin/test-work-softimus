<head>
    <title>TEST</title>
    <link rel="stylesheet" href="/bootstrap.min.css">
    <script src="/jq.js"></script>
    <style>
        .text-success{
            font-size: 1.5em;
            font-weight: bold;
        }
    </style>
</head>
<body>
<h2 class="text-center text-info">Collection</h2>
    <div class="container">
        <a class="btn btn-primary" href="/add">+ append new item</a>
        <div class="row" id="collection">
        </div>
    </div>

<script>
    var item = function(model){
        var properties = '';
        if(model.properties){
            $.each(model.properties,function(k,el){
                if (el.type == '1') {
                    properties = properties + '<b>'+el.name+': </b> <span> ' +el.value +'</span><br/>';
                } else if (el.type == '2') {
                    properties = properties + '<b>'+el.name+': </b> <textarea readonly class="form-control" rows="5"> ' +el.value + '</textarea><br/>';
                } else if (el.type == '3') {
                    if(el.value == '1'){
                        el.value = 'Y'
                    } else {
                        el.value = 'N'
                    }
                    properties = properties +'<b>'+el.name+': </b> ' + el.value +'</br>';
                } else if (el.type == '4') {
                    properties = properties +'<b>'+el.name+': </b> ' + el.value +'</br>';
                } else if (el.type == '5') {
                    properties = properties + '<b>'+el.name+': </b> <br/>';
                    $.ajax({
                        url: "/load_variant",
                        dataType: "json",
                        async:false,
                        success: function (data) {
                            $.each(data, function (k, v) {
                                var cheker = 'text-mutted';
                                console.log(k,el.value,(k == el.value));
                                if(k == el.value){
                                    cheker = 'text-success';
                                }
                                properties = properties + '<span class="'+cheker+'">' + v + '</span><br/>'
                            });
                        }
                    });
                }

            });
        }
        if(properties){
            properties = '<h4>Properties:</h4>' + properties;
        }
        return '<div class="col-xs-12 panel panel-info" datatype="'+model.id+'">' +
                '<b>Name: </b><span>'+model.name+'</span></br>'+
                '<b>cost: </b><span>'+model.cost+'</span></br>'+
                '<b>Created: </b><span>'+model.date+'</span></br>'+
                properties+
            '</div>';
    }

    function load(){
        $.ajax({
            url:'/all',
            type:'get',
            dataType:"json",
            success:function(response){
                if(response.collection){
                    $("#collection").empty();
                    $.each(response.collection,function(k,v){
                        $('#collection').append(item(v));
                        $('.panel').last().on('dblclick',function(){
                            console.log($(this));
                            redoItem($(this));
                        })
                    });
                }
            }
        });
    }
    function redoItem(self) {
        if(confirm('Do you want redo item')){
            $(location).attr('href', '/redo/'+self.attr('datatype'));
        }
    }
    $(document).ready(function(){
        load();
        setInterval(function () {
        load()
        },30000);

    })
</script>
</body>