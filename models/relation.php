<?php
require_once 'model.php';
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 31.08.2016
 * Time: 21:35
 */
class relation extends model
{
    protected $table='types_to_stuff';

    static public $types = [
        1 => 'text_val',
        2 => 'long_text_val',
        3 => 'bool_val',
        4 => 'int_val',
        5 => 'int_val'
    ];
}