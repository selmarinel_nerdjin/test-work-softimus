<?php
require_once 'model.php';
require_once ("{$base_dir}models{$ds}relation.php");
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 31.08.2016
 * Time: 21:26
 */
class stuff extends model
{
    protected $table='stuff';

    public function loadRelate($id)
    {
        $relate = new relation();
        $collection = $relate->query(['stuff_id'=>$id]);
        return $collection;
    }
}