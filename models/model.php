<?php
$ds = DIRECTORY_SEPARATOR;
$base_dir = realpath(dirname(__FILE__) . $ds . '..') . $ds;
require_once("{$base_dir}controllers{$ds}data.php");

class model extends data
{

    protected $table = 'base';
    protected $pk = 'id';

    public function all()
    {
        $link = self::connect();
        $resource = mysqli_query($link, 'SELECT * FROM ' . $this->table);
        $result_array = array();
        while ($row = mysqli_fetch_array($resource, MYSQLI_ASSOC)) {
            $result_array[] = $row;
        }
        mysqli_free_result($resource);
        return $result_array;
    }

    public function create(Array $data)
    {
        $link = self::connect();
        $rows = array_keys($data);
        $values = array_values($data);
        $sql = 'INSERT INTO ' . $this->table . '(' . implode(', ', $rows) . ')
         VALUES ("' . implode('", "', $values) . '")';
        mysqli_query($link, $sql);

        return mysqli_insert_id($link);
    }

    public function find($id)
    {
        $link = self::connect();
        $resource = mysqli_query($link, 'SELECT * FROM ' . $this->table . ' WHERE ' . $this->pk . '=' . $id);
        $result_array = array();
        while ($row = mysqli_fetch_array($resource, MYSQLI_ASSOC)) {
            $result_array[] = $row;
        }
        mysqli_free_result($resource);
        return $result_array[0];
    }

    public function query(Array $data)
    {
        $link = self::connect();
        $query = [];
        foreach ($data as $key => $value) {
            if (is_numeric($value)) {
                $query[] = " {$key} = {$value} ";
            } else {
                $query[] = " {$key} = '{$value}' ";
            }
        }
        $resource = mysqli_query($link, 'SELECT * FROM ' . $this->table . ' WHERE ' . implode(' AND ', $query));
        $result_array = array();
        while ($row = mysqli_fetch_array($resource, MYSQLI_ASSOC)) {
            $result_array[] = $row;
        }
        mysqli_free_result($resource);
        return $result_array;
    }

    public function update(Array $data, $id)
    {
        $link = self::connect();
        $query = [];
        foreach ($data as $key => $value) {
            if (is_numeric($value)) {
                $query[] = " {$key} = {$value} ";
            } else {
                $query[] = " {$key} = '{$value}' ";
            }
        }
        $resource = mysqli_query($link, 'UPDATE ' . $this->table .
            ' SET ' . implode(', ', $query) . ' WHERE ' . $this->pk . '=' . $id);
        return $id;
    }

    public function delete($id)
    {
        $link = self::connect();
        $resource = mysqli_query($link, 'DELETE FROM  ' . $this->table . ' WHERE ' . $this->pk . '=' . $id);
        return $id;
    }
    
    
}

