<?php
include 'data.php';

require_once("{$base_dir}models{$ds}variants.php");
require_once("{$base_dir}models{$ds}stuff.php");
require_once("{$base_dir}models{$ds}relation.php");

function display($varArray = array(), $_layout_name = '_layouts/default')
{
    $_layout_name = "views/{$_layout_name}.low.php";
    $data = $varArray;
    ob_start();
    include $_layout_name;
}

function index()
{
    data::createBase();
    display();
}

function deleteRelate($id)
{
    $relate = new relation();
    $relate->delete($id);
    header("Content-Type: application/json");
    echo json_encode(['success' => 'Deleted']);
}

function redoItem($id)
{
    if (!empty($_POST)) {
        $request = $_POST;
        if (htmlspecialchars($request['name']) && $request['cost'] && is_numeric($request['cost'])) {
            $stuff = new stuff();
            $stuff->update(
                [
                    'name' => htmlspecialchars($request['name']),
                    'cost' => htmlspecialchars($request['cost'])
                ]
                , $id);
            if ($request['type'] && !empty($request['type'])) {
                $relate = new relation();
                foreach ($request['type'] as $key => $types) {
                    switch ($types) {
                        case 1:
                            $type = 'text_val';
                            break;
                        case 2:
                            $type = 'long_text_val';
                            break;
                        case 3:
                            $type = 'bool_val';
                            $request['type-input'][$key] = ($request['type-input'][$key]) ? 1 : 0;
                            break;
                        case 4:
                            $type = 'int_val';
                            break;
                        case 5:
                            $type = 'int_val';
                            break;
                        default:
                            $type = '';
                    }
                    if ($request['relate_id'][$key]) {
                        $relate->update([
                                'type_id' => $types,
                                $type => htmlspecialchars($request['type-input'][$key]),
                                'name' => htmlspecialchars($request['relate_name'][$key]),
                            ]
                            , $request['relate_id'][$key]);
                    } else {
                        $relate->create([
                            'stuff_id' => $id,
                            'type_id' => $types,
                            'name' => htmlspecialchars($request['relate_name'][$key]),
                            $type => htmlspecialchars($request['type-input'][$key])
                        ]);
                    }
                }
            }
            header("Content-Type: application/json");
            echo json_encode(['goto' => '/']);
            return;
        } else {
            header("Content-Type: application/json");
            echo json_encode(['error' => 'Check input data']);
            return;
        }
    }
    $stuff = new stuff();
    $model = $stuff->find($id);
    display($model, 'add');
}

function appendItem()
{
    if (!empty($_POST)) {
        $request = $_POST;
        if ($request['name'] && $request['cost'] && is_numeric($request['cost'])) {
            $stuff = new stuff();
            $id = $stuff->create([
                'name' => htmlspecialchars($request['name']),
                'cost' => htmlspecialchars($request['cost']),
            ]);
            if ($request['type'] && !empty($request['type'])) {
                $relation = new relation();
                foreach ($request['type'] as $key => $types) {
                    switch ($types) {
                        case 1:
                            $type = 'text_val';
                            break;
                        case 2:
                            $type = 'long_text_val';
                            break;
                        case 3:
                            $type = 'bool_val';
                            $request['type-input'][$key] = ($request['type-input'][$key]) ? 1 : 0;
                            break;
                        case 4:
                            $type = 'int_val';
                            break;
                        case 5:
                            $type = 'int_val';
                            break;
                        default:
                            $type = '';
                    }
                    $relation->create([
                        'stuff_id' => $id,
                        'type_id' => $types,
                        'name' => htmlspecialchars($request['relate_name'][$key]),
                        $type => htmlspecialchars($request['type-input'][$key])
                    ]);
                }
            }
        } else {
            header("Content-Type: application/json");
            echo json_encode(['error' => 'Check input data']);
            return;
        }
        header("Content-Type: application/json");
        echo json_encode(['goto' => '/']);
        return;
    }
    display([], 'add');
}

function loadRelate($id)
{
    $relate = new relation();
    $collection = $relate->query(['stuff_id' => $id]);
    $result = [];
    foreach ($collection as $model) {
        $result[] = [
            'id' => $model['id'],
            'stuff_id' => $model['stuff_id'],
            'type_id' => $model['type_id'],
            'name' => $model['name'],
            'value' => $model[relation::$types[$model['type_id']]]
        ];
    }
    header("Content-Type: application/json");
    echo json_encode($result);

}

function variantList()
{
    $link = data::connect();
    $variant = new variants();
    $collection = $variant->all();
    $result = [];
    array_map(function ($model) use (&$result) {
        $result[$model['id']] = $model['name'];
    }, $collection);
    header("Content-Type: application/json");
    echo json_encode($result);
}

function loadAll()
{
    header("Content-Type: application/json");
    $stuff = new stuff();
    $collection = $stuff->all();
    $result = [];
    if ($collection) {
        foreach ($collection as $model) {
            $result[] = getElement($model['id']);
        }
    }
    echo json_encode(['collection' => $result]);
    return true;
}

function getElement($id)
{
    $stuff = new stuff();
    $model = $stuff->find($id);
    if (!$model) {
        return false;
    }
    $relate = $stuff->loadRelate($id);
    $result = [
        'id' => $model['id'],
        'name' => $model['name'],
        'cost' => $model['cost'],
        'date' => $model['created_at']
    ];
    if ($relate) {
        foreach ($relate as $property) {
            $result['properties'][] = [
                'name' => $property['name'],
                'type' => $property['type_id'],
                'value' => $property[relation::$types[$property['type_id']]]
            ];
        }
    }
    return $result;
}