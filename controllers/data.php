<?php
$ds = DIRECTORY_SEPARATOR;
$base_dir = realpath(dirname(__FILE__)  . $ds . '..') . $ds;
class data
{
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '123';
    const DB_NAME = 'test_job';
    const DB_DRIVER = 'mysql';

    static public function connect()
    {
        $link = mysqli_connect(self::DB_HOST, self::DB_USER, self::DB_PASS);
        if ($link) {
            mysqli_select_db($link, self::DB_NAME);
        }
        return $link;
    }

    static public function createBase()
    {
        $link = self::connect();

        $CreateTableStuff = "CREATE TABLE IF NOT EXISTS `stuff` 
( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(255) NOT NULL ,
  `cost` FLOAT NOT NULL ,
   `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;";

        $CreateTableTypes = "CREATE TABLE IF NOT EXISTS `types`
 ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`)) ENGINE = InnoDB;";

        $CreateTableTypesToStuff = "CREATE TABLE IF NOT EXISTS `types_to_stuff`
 ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `stuff_id` INT UNSIGNED NOT NULL ,
   `type_id` INT UNSIGNED NOT NULL ,
   `name` VARCHAR(255) DEFAULT property,
    `text_val` VARCHAR(255) DEFAULT NULL ,
     `long_text_val` TEXT DEFAULT NULL ,
      `bool_val` BOOLEAN DEFAULT NULL ,
        `int_val` INT DEFAULT NULL ,
       PRIMARY KEY (`id`)) ENGINE = InnoDB;";

        $CreateTableVariant = "CREATE TABLE IF NOT EXISTS `select_variants` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(200) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";

        $truncateTypes = 'TRUNCATE TABLE `types`;';
        $createTypes = "
  INSERT INTO `types` (`id`, `name`) VALUES (NULL, 'short_text'), (NULL, 'long_text'), (NULL, 'boolean'),(NULL ,'int'),(NULL ,'select')  ;
";

        $truncateVariants = 'TRUNCATE TABLE `select_variants`;';
        $createVariants = "
  INSERT INTO `select_variants` (`id`, `name`) VALUES (NULL, 'var 1'), (NULL, 'var 2'), (NULL, 'var 3'),(NULL ,'var 4') ;
";
        mysqli_query($link,$CreateTableStuff);
        mysqli_query($link,$CreateTableTypes);
        mysqli_query($link,$CreateTableTypesToStuff);
        mysqli_query($link,$CreateTableVariant);
        mysqli_query($link,$truncateTypes);
        mysqli_query($link,$createTypes);
        mysqli_query($link,$truncateVariants);
        mysqli_query($link,$createVariants);
    }
}